import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class PropertieHandler
{
    private String USER;
    private String PASS;

    public PropertieHandler()
    {
        load();
    }

    public void store(String userName, String passWord)
    {
        System.out.println("running");
        Properties props = new Properties();
        props.setProperty("jdbc.username", userName);
        props.setProperty("jdbc.password", passWord);

        FileOutputStream out = null;
        try
        {
            out = new FileOutputStream("database.properties");
            props.store(out, "Database info");
            out.close();
        }
        catch (Exception e)
        {

        }
    }

    public void load()
    {

        Properties props = new Properties();

        FileInputStream in = null;
        try
        {
            in = new FileInputStream("database.properties");
            props.load(in);
            props.list(System.out);
            in.close();
        }
        catch (Exception e)
        {
        }

        this.USER = props.getProperty("jdbc.username");
        this.PASS = props.getProperty("jdbc.password");
    }

    public String getUser()
    {
        return this.USER;
    }

    public String getPASS()
    {
        return this.PASS;
    }
}
